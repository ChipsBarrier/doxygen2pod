package Doxygen::DocBlock;


#** @class Doxygen::DocBlock
#
# Simple data container for a block of doxygen commentary.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Carp 'croak';
use namespace::clean;
use Moo;
use Types::Standard qw/ Int Str Bool ArrayRef Enum /;

has 'package'    => ( is => 'ro', isa => Str, required => 1, );
has 'name'       => ( is => 'ro', isa => Str, required => 1, );
has 'type'       => ( is => 'ro',
                     isa => Enum[qw/ file class function method constructor var /],
                     required => 1, );
has 'first_line' => ( is => 'ro', isa => Int, required => 1 );
has 'last_line'  => ( is => 'rw', isa => Int, required => 0 );
has 'def_line'   => ( is => 'rw', isa => Int, required => 0 );
has 'doc'        => ( is => 'ro', isa => ArrayRef[Str], required => 0,
                      default => sub { [] } );
has 'private'    => ( is => 'rw', isa => Bool, required => 0, default => 0 );

# brief comes without leading or trailing whitespace:
has 'brief'      => ( is => 'ro', isa => Str, required => 0,
                      lazy => 1,
                      builder => 1 );
has 'header_params' => ( is => 'ro', isa => ArrayRef, required => 0 );
has 'params'        => ( is => 'rw', isa => ArrayRef, required => 0 );

### @TAG:
has 'warning'       => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'note'          => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'return'        => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'see'           => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'throws'        => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'author'        => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'copyright'     => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'todo'          => ( is => 'rw', isa => ArrayRef, required => 0 );
has 'inherits'      => ( is => 'rw', isa => ArrayRef, required => 0 );


sub addParam {
    my( $self, $param ) = @_;
    push @{$self->{params}}, $param;
}

sub addTag {
    my( $self, $tag, $lines ) = @_;

    if(    'return'    eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'see'       eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'todo'      eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'note'      eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'throws'    eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'warning'   eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'copyright' eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'author'    eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    elsif( 'inherits'  eq $tag ) { push @{ $self->{$tag} }, @$lines; }
    else {
        croak( "'$tag': No such tag!" );
    }
}

#** @method addDocLine ( $line )
#
# Add a line of text to the \c 'doc' field of this DocBlock.
#*
sub addDocLine {
    my( $self, $line ) = @_;
    push @{$self->{doc}}, $line;
}

#** @method _build_brief ()
#
# Builder for the \c 'brief' description of a documented element.  The brief is
# always the first sentence found.
#
# @return The generated \c 'brief' description of the element.
#*
sub _build_brief {
    my $self = shift;
    my $brief = '';
    local $1;
    for my $line ( $self->{doc}->@* )
    {
        next unless( $line =~ /[^\s]+/o ); # Skip empty lines;
        $brief .= $line;
        if( $brief =~ /\A([^.!?]+[.!?])/mo ) {
            $brief = $1;
            last;
        }
    }

    return _trim($brief);
}


sub _trim {
    my $x = pop;
    $x =~ s/(?:\A\s*|\s*\z)//ogm;
    return $x;
}

# sub _rtrim {
#     my $lines = pop;
#     if( 'ARRAY' eq ref $lines ) {
#         map { $_ =~ s/\s*\z//mo } @$lines;
#         return @$lines;
#     }
#     else {
#         $lines =~ s/\s*\z//o;
#         return $lines;
#     }
# }

1;
