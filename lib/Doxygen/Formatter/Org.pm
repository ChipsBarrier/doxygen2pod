package Doxygen::Formatter::Org;


#** @class Doxygen::Formatter::Org
#
# Format output as Perl Org-Mode.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Carp;
use feature ':5.24';
use Cwd 'abs_path';

use parent 'Doxygen::Formatter';

#** @method new ( )
#
# Create a new Instance of Doxygen::Formatter::Org.
#
#*
sub new
{
    my( $class, ) = @_;
    my $self = {
        source_root => '',
        odd_even => 0,
    };
    bless $self, $class;
}

sub outputFilenameExtension {
    return 'org';
}

sub beginDocument {
    my $self = shift;
    say "#+OPTIONS: ^:nil toc:3";
    say '#+HTML: <style type="text/css">';
    # Module Names in TOC:
    # say '#+HTML:    div#text-table-of-contents > ul > li > a { font-weight: bold; font-size: 130%;}';
    say '#+HTML:    div.outline-2 h2 { font-size: 300%; }';
    # Hide title and section numbers:
    say '#+HTML:    h1.title, span.section-number-2, ';
    say '#+HTML:    span.section-number-3, span.section-number-4, ';
    say '#+HTML:    span.section-number-5, span.section-number-6, ';
    say '#+HTML:    span.toc-number-1, span.toc-number-2, span.toc-number-3, ';
    say '#+HTML:    span.toc-number-4, span.toc-number-5, span.toc-number-6 ';
    say '#+HTML:      { display:none; }';
    say '#+HTML:    #text-table-of-contents li a,';
    say '#+HTML:    #text-table-of-contents li a span,';
    say '#+HTML:    #text-table-of-contents li a code';
    say '#+HTML:      { white-space: nowrap; }';
    say '#+HTML:    div.outline-5 h5 { margin-bottom: 0px; }';
    say '#+HTML:    div.outline-text-5 { padding-left: 1em; }';
    say '#+HTML:    div.outline-text-4 { padding-left: 1.5em; }';
    # say '#+HTML:    div.outline-text-3 { border-bottom:1px solid gray } ';
    say '#+HTML: </style>';
    say '';
}

sub endDocument {
    # Nothing to be done
}

sub _sectionProps {
    my( $self, $level ) = @_;

    $level = ($level * 2 -1)  unless $self->{odd_even};
    my $stars = ("*" x $level) . ' ';
    ( my $ind = $stars ) =~ tr/*/ /;
    return( $stars, $ind );
}


sub formatTags {
    my($self, $dox) = @_;
    my $content;

    my( $stars, $ind ) = $self->_sectionProps(4);
    for my $tag ( qw/throws warning note see author copyright todo/ )
    {
        if( $content = $dox->$tag() ) {
            say $stars, ucfirst $tag;
            $self->formatDoxygen( $ind, $content );
            say '';
        }
    }
}

sub formatFile {

    my( $self, $dox ) = @_;

    my( $stars, $ind ) = $self->_sectionProps(1);

    say $stars, $self->srcBasename();
    say $ind, ':PROPERTIES:';
    say $ind, '  :CUSTOM_ID: ', $self->srcBasename();
    say $ind, ':END:';
    say '';

    $self->formatDoxygen( $ind, $dox->{doc} );
    $self->formatTags( $dox );
}

sub formatPackage {

    my( $self, $dox ) = @_;

    my( $stars, $ind ) = $self->_sectionProps(1);

    say $stars, $dox->package;
    say $ind, ':PROPERTIES:';
    # Prefix 'package_' - subroutines might have the same name!
    say $ind, '  :CUSTOM_ID: ', 'package-', $dox->package;
    say $ind, ':END:';
    say '';

    $self->formatDoxygen( $ind, $dox->doc );
    $self->formatTags( $dox );
}



sub formatSection {
    my( $self, $headline ) = @_;
    my( $stars, $ind ) = $self->_sectionProps(2);
    say "$stars $headline";

    # We know, that sections are are uniq per file, if there's only one package.
    my $mark = $self->markCurrentPackage
        ? $self->currentPackage . '-'
        : '';
    say $ind, ':PROPERTIES:';
    say $ind, '  :CUSTOM_ID: ', $mark, 'sec-', lc $headline;
    say $ind, ':END:';
    say '';
}


sub formatSubroutine {

    my( $self, $dox ) = @_;

    my ( $stars, $ind ) = $self->_sectionProps(3);

    say $stars, '~', $dox->name, ' (',
        ( defined($dox->header_params)
          ? ' ' . join(', ', @{$dox->header_params}) . ' '
          : '' ),
              ")~";

    # Uniq per file if there's only one package:
    my $mark = $self->markCurrentPackage ? $self->currentPackage . '::' : '';
    say $ind, ':PROPERTIES:';
    say $ind, '  :CUSTOM_ID: ', $mark, $dox->name;
    say $ind, ':END:';
    say '';

    $self->formatDoxygen($ind, $dox->doc);

    if ( $dox->{params} )
    {
        my( $stars, $ind ) = $self->_sectionProps(4);
        say $stars, 'Parameters';
        for my $y ( $dox->params()->@* )
        {
            print $ind, '* ~', $y->{sigil}, $y->{name}, '~ ::';
            $self->formatDoxygen( "$ind  ", $y->{doc} );
        }
        say '';
    }
    if ( $dox->return )
    {
        my( $stars, $ind ) = $self->_sectionProps(4);
        say $stars, 'Return';
        $self->formatDoxygen( $ind, $dox->return );
        say '';
    }

    $self->formatTags( $dox );

    if ( $dox->{def_line} )
    {
        say '#+HTML: <div style="font-size:70%;text-align:right;padding:7px 7px;">Source: ',
            $self->relativeSrcFilename,
            ', line ', $dox->def_line, ' ff</div>';
        say '';
    }

}


#### FORMAT VARIABLES ####
sub formatVariable {
    my($self, $dox) = @_;

    my( $stars, $ind ) = $self->_sectionProps(3);
    say $stars, " ~$dox->{name}~\n";
    $self->formatDoxygen( $ind, $dox->doc );
}


#### FORMAT UNTAGGED DOXYGEN ####
sub formatDoxygen {

    my( $self, $ind, $lines ) = @_;
    my $line;

    while( defined( $line = shift @$lines ) )    # DESTRUCTIVE !!!
    {
        if ( $line =~ /\s*(?:[@\\]code\b|<pre>)/o ) # FIXME: Move this stuff to a Doxygen::Block class or similar!!
        {
            say $ind, '#+BEGIN_SRC perl';
            while ( defined( $line = shift @$lines ) )
            {
                if ( $line =~ /\s*(?:[@\\]endcode|<\/pre>)/o )
                {
                    last;
                }
                print "$ind  ", $line;
            }
            say $ind, '#+END_SRC';
            say '';
        }

        else                            # All other lines shall be trimmed...
        {
            $line =~ s/^\h*//o;
            if ( $line =~ /^\@([a-z]+)\h+(.*)/so )
            {
                say '';
                my $tag = ucfirst $1;
                my $content = $2;
                print $ind, '*', ucfirst $tag, '*: ', $self->emph($content);
                while ( $lines->[0] =~ /\W+/o )
                {
                    print $ind, $self->emph( shift(@$lines) );
                }
                say '';
            }
            else
            {
                print $ind, $self->emph( $line );
            }
        }
    }
    say '';             # empty line required - maybe
}


#** @method emph ( $line )
#
# Take a line of doxygen comment and convert all emphasis marks found in that
# line to Org.  See Doxygen::Formatter and DoxygenToPod.
#
# @return The converted text.
#*
sub emph {
    my( $self, $line ) = @_;
    local($1, $2, $3, $4);

    my $s = '';
    my $end = length($line);

    while( pos($line) < $end )
    {

        # if( $line =~ /\G ( (?:[a-zA-Z0-9]+) (?: ::[a-zA-Z0-9]+)*\b )/xgco )
        # {
        #     my $mod = $1;

        #     say STDERR $mod;
        #     say STDERR $self->currentPackage;
        #     if ( $mod eq $self->currentPackage )
        #     {
        #         say STDERR "Skipping a link to myself";
        #         # Not an external link.
        #         $s .= "~$mod~";
        #     }
        #     else
        #     {
        #         my $found = 0;

        #         say STDERR $self->absoluteSrcFilename;
        #         say STDERR $self->relativeSrcFilename;

        #         my $my_abs = $self->absoluteSrcFilename;
        #         my $my_rel = $self->relativeSrcFilename;
        #         my $root   = substr($my_abs, 0,
        #                             length($my_abs) - length($my_rel) -1
        #                         );

        #         # Now let's see if we find the link target:
        #         ( my $supposed_target = "$mod.pm" ) = s/::/\//g;

        #         # Guess the target filename. Chances are quite good to match it.
        #         if ( -f "$root/$supposed_target" )
        #         {
        #             # Let's say an Org file will be generated for
        #             # $supposed_target, too, and link to that:
        #             my $url = ("../" x ($my_rel =~ tr/\///c)) . "$mod.pm";
        #             $url =~ s/::/\//g;
        #             $s .= "[[$url][$mod]]";
        #         }
        #         else
        #         {
        #             # Give up. Create a 'perldoc:' link:
        #             $s .= "[[perldoc:$mod][$mod]]";
        #         }
        #     }
        # }
        $s .= $1     if $line =~ /\G ([^\\\@<]+)/xgco;
        $s .= $1     if $line =~ /\G \\([\@\\&~<>#%]) /xgco; # Backslash escapes

        $s .= '*' . $1 . '*' if $line =~ /\G <b>([^<]*)<\/b>/xgco;
        $s .= '*' . $1 . '*' if $line =~ /\G [\@\\]b\h+([^\s]+)/xgco;

        $s .= '/' . $1 . '/' if $line =~ /\G <i>([^<]*)<\/i>/xgco;
        $s .= '/' . $1 . '/' if $line =~ /\G [\@\\](?:a|e|em)\h+([^\s]+)/xgco;

        $s .= '_' . $1 . '_' if $line =~ /\G <u>([^<]*)<\/u>/xgco;
        $s .= '_' . $1 . '_' if $line =~ /\G [\@\\]u\h+([^\s]+)/xgco;

        $s .= '~' . $1 . '~' if $line =~ /\G <tt>([^<]*)<\/tt>/xgco;
        $s .= '~' . $1 . '~' if $line =~ /\G <code>([^<]*)<\/code>/xgco;
        $s .= '~' . $1 . '~' if $line =~ /\G [\@\\][cp]\h+([^\s]+)/xgco;

        $s .= "\n\n"         if $line =~ /\G \\par/xgco;
        $s .= "#+BEGIN_QUOTE\n" if $line =~ /\G <blockquote> /xgco;
        $s .= "#+END_QUOTE\n"   if $line =~ /\G <\/blockquote> /xgco;
        $s .= '_*' . $1 . '*_' if $line =~ /\G <h3>([^<]*)<\/h3>/xgco;

        # Fallback: Other character without a meaning (e.g a space):
        $s .= $1              if $line =~ /\G (.)/xgco;
    }

    return $s;
}



1;
