package Doxygen::Formatter::Pod;


#** @class Doxygen::Formatter::Pod
#
# Format output as Perl Pod.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Carp;
use feature ':5.24';

use parent 'Doxygen::Formatter';

#** @method new ( )
#
# Create a new Instance of Doxygen::Formatter::Pod.
#
#*
sub new
{
    my( $class, %options ) = @_;
    my $self = {
        relative_filename => '',
        absolute_filename => '',
        over_h1   => 4,
        over_h2   => 4,
        over_list => 8,
        current_package => '',
        mark_package => 0,
    };
    bless $self, $class;

    $self->setOverH1(   $options{over_h1}   ) if exists $options{over_h1};
    $self->setOverH2(   $options{over_h2}   ) if exists $options{over_h2};
    $self->setOverList( $options{over_list} ) if exists $options{over_list};

    $self;
}
#** @method setOverH1 ( $over_depth )
#
# We insert an <code>=over N</code> after each \c =head1.  You may adjust the
# \c N or skip the insertion for good by giving a \c 0.
#
# @param over_depth Parameter given to the \c =over following \c =head1.
#*
sub setOverH1 {
    my( $self,  $over_depth  ) = @_;
    $self->{over_h1} = int($over_depth);
}

#** @method setOverH2 ( $over_depth )
#
# We insert an <code>=over N</code> after each \cn =head2.  You may adjust the
# \c N or skip the insertion for good by giving a \c 0.
#
# @param over_depth Parameter given to the \c =over following \c =head2.
#*
sub setOverH2 {
    my( $self,  $over_depth  ) = @_;
    $self->{over_h2} = int($over_depth);
}

#** @method setOverList ( $over_depth )
#
# Set the parameter to the <code>=over N</code> preceeding lists.
# Must be a positive integer!
#
# @param over_depth Parameter given to the \c =over preceding lists.
#*
sub setOverList {
    my( $self,  $over_depth  ) = @_;
    my $x = int($over_depth);
    unless( $x && $x > 0 ) {
        warn "[WARN] '$over_depth': no positive integer given for list indentation!  Using default.";
        return;
    }
    $self->{over_list} = $x;
}

sub currentPackage {
    my( $self, $name) = @_;
    $self->{current_package} = $name if defined $name;
    return $self->{current_package};
}

sub markCurrentPackage {
    my( $self, $mark) = @_;
    $self->{mark_package} = $mark if defined $mark;
    return $self->{mark_package};
}

sub setCurrentPackage {
    my( $self, $package_name, $mark_package ) = @_;
    $self->currentPackage($package_name);
    $self->markCurrentPackage($mark_package);
}


sub outputFilenameExtension {
    return 'pod';
}

sub sourceFilename {
    my( $self, %params ) = @_;
    $self->relativeFilename( $params{rel} );
    $self->absoluteFilename( $params{abs} );
    return $self->relativeFilename;
}

sub relativeFilename {
    my( $self, $name ) = @_;
    $self->{relative_filename} = $name  if defined $name;
    return $self->{relative_filename};
}

sub absoluteFilename {
    my( $self, $name ) = @_;
    $self->{absolute_filename} = $name  if defined $name;
    return $self->{absolute_filename};
}


sub beginDocument {
    say "=encoding utf8\n";
    say "=pod\n";
}

sub endDocument {
    say "=cut\n";
}

sub formatTags {
    my($self, $dox) = @_;
    my $content;
    for my $tag ( qw/throws warning note see author copyright todo/ )
    {
        if( $content = $dox->$tag() ) {
            say 'B<' . ucfirst($tag) .":>";
            $self->formatDoxygen( $content );
        }
    }
}


sub formatFile {

    my( $self, $p ) = @_;

    say "=head1 FILE $p->{package}\n";
    say "=over $self->{over_h1}\n"      if $self->{over_h1};
    $self->formatDoxygen( $p->{doc} );
    $self->formatTags( $p );
    say "=back\n"                       if $self->{over_h1};
}


sub formatPackage {

    my( $self, $p ) = @_;

    say "=head1 NAME\n";
    say "=over $self->{over_h1}\n"      if $self->{over_h1};
    say $p->{package} . "\n";
    say "=back\n"                       if $self->{over_h1};
    say "=head1 DESCRIPTION\n";
    say "=over $self->{over_h1}\n"      if $self->{over_h1};
    $self->formatDoxygen( $p->{doc} );
    $self->formatTags( $p );
    say "=back\n"                       if $self->{over_h1};
}


sub formatSection {
    my( $self, $headline ) = @_;
    say "=head1 $headline\n\n";
}


sub formatSubroutine {

    my( $self, $f ) = @_;

    say "=head2 $f->{name} (",
        ( defined($f->{header_params})
          ? ' ' . join(', ', @{$f->{header_params}}) . ' '
          : '' ),
              ")\nX<$f->{name}>\n";

    say "=over $self->{over_h2}\n"       if $self->{over_h2};

    $self->formatDoxygen($f->{doc});

    if ( $f->{params} )
    {
        say "B<Parameters:>\n";
        say "=over $self->{over_list}\n";
        for my $y ( @{$f->{params}} )
        {
            say '=item C<', $y->{sigil}, $y->{name}, '>', "\n"; # Extra newline!
            $self->formatDoxygen( $y->{doc} );
        }
        say "=back\n";
    }
    if ( $f->return )
    {
        say "B<Return:> ";
        $self->formatDoxygen( $f->return ),
    }
    if ( $f->{def_line} )
    {
        say "=begin html\n";
        say '<div class="definitionAt">Source: ',
            $self->sourceFilename,
            ", line $f->{def_line} ff</div><br />\n";
        say "=end html\n";
    }
    $self->formatTags( $f );
    say "=back\n"       if $self->{over_h2};
}


#### FORMAT UNTAGGED DOXYGEN ####
sub formatDoxygen {

    my( $self, $dox ) = @_;
        my $line;

    while( defined( $line = shift @$dox ) )
    {
        if ( $line =~ /\s*(?:[@\\]code\b|<pre>)/o ) # FIXME: Move this stuff to a Doxygen::Block class!!
        {
            say '';
            while ( defined( $line = shift @$dox ) )
            {
                if ( $line =~ /\s*(?:[@\\]endcode|<\/pre>)/o )
                {
                    say '';             # empty line required
                    last;
                }
                print "    $line";
            }
        }

        else                            # All other lines shall be trimmed...
        {
            $line =~ s/^\h*//o;
            if ( $line =~ /^\@([a-z]+)\h+(.*)/so )
            {
                say '';
                print 'B<', ucfirst($1), ':> ', emph($2);
                while ( $dox->[0] =~ /\W+/o )
                {
                    print emph( shift(@$dox) );
                }
                say '';
            }
            else
            {
                print emph( $line );
            }
        }
    }
    say '';             # empty line required - maybe
}


#### FORMAT VARIABLES ####
sub formatVariable {
    my($self, $dox) = @_;

    say "=head2 C<$dox->{name}\n";
    say join('',
             map { emph($_) } @{$dox->{doc}}
         ), "\n"
             if $dox->{doc};
}


#** @function emph ( $line )
#
# Take a line of doxygen comment and convert all emphasis marks found in that
# line to POD.
#
# @return The converted text.
#*
sub emph {

    my $line = pop();
    local($1, $2, $3, $4);

    my $s = '';
    my $end = length($line);

    while( pos($line) < $end )
    {
        $s .= $1     if $line =~ /\G ([^\\\@<]+)/xgco;
        $s .= $1     if $line =~ /\G \\([\@\\&~<>#%]) /xgco; # Backslash escapes

        $s .= 'B<<< ' . esc($1) . ' >>>' if $line =~ /\G <b>([^<]*)<\/b>/xgco;
        $s .= 'B<<< ' . esc($1) . ' >>>' if $line =~ /\G [\@\\]b\h+([^\s]+)/xgco;

        $s .= 'I<<< ' . esc($1) . ' >>>' if $line =~ /\G <i>([^<]*)<\/i>/xgco;
        $s .= 'I<<< ' . esc($1) . ' >>>' if $line =~ /\G [\@\\](?:a|e|em)\h+([^\s]+)/xgco;

        $s .= 'U<<< ' . esc($1) . ' >>>' if $line =~ /\G <u>([^<]*)<\/u>/xgco;
        $s .= 'U<<< ' . esc($1) . ' >>>' if $line =~ /\G [\@\\]u\h+([^\s]+)/xgco;

        $s .= 'C<<< ' . esc($1) . ' >>>' if $line =~ /\G <tt>([^<]*)<\/tt>/xgco;
        $s .= 'C<<< ' . esc($1) . ' >>>' if $line =~ /\G <code>([^<]*)<\/code>/xgco;
        $s .= 'C<<< ' . esc($1) . ' >>>' if $line =~ /\G [\@\\][cp]\h+([^\s]+)/xgco;


        # Fallback: Special character without a meaning:
        $s .= esc($1)              if $line =~ /\G (.)/xgco;
    }

    return $s;
}

#** @function esc ( $str )
#
# Escape special characters for POD. Currently `|' only.
#
# @param str A string to escape.
# @return The escaped string.
#*
sub esc {
    my $x = pop();
    $x =~ s/\|/E<verbar>/go;
    $x =~ s/\\\\/\\/g;        # Backslash escapes
    $x;
}


1;
