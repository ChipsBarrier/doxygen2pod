package Doxygen::Formatter;


#** @class Doxygen::Formatter
#
# Output formatter super class.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*

use strict;
use utf8;
use File::Spec;


sub forString {
    my( $class, $name ) = @_;
    $name = $class     unless $name;    # Static call.

    my $package = __PACKAGE__ . '::' . ucfirst $name;
    ( my $path = $package . '.pm' ) =~ s/::/\//g;

    require $path;
    my $obj = $package->new();
    $obj->{current_package} = '';
    $obj->{mark_package} = 0;
    $obj->{src_absolute_filename} = '';
    $obj->{src_relative_filename} = '';
    $obj->{src_basename} = '';
    return $obj;
}

sub setSourceFilename {
    my( $self, %params ) = @_;
    $self->relativeSrcFilename( $params{rel} );
    $self->absoluteSrcFilename( $params{abs} );
    $self->srcBasename( $params{abs} );
}


sub setCurrentPackage {
    my( $self, $package_name, $mark_package ) = @_;
    $self->currentPackage($package_name);
    $self->markCurrentPackage($mark_package);
}

sub currentPackage {
    my( $self, $name) = @_;
    $self->{current_package} = $name if defined $name;
    return $self->{current_package};
}

sub markCurrentPackage {
    my( $self, $mark) = @_;
    $self->{mark_package} = $mark if defined $mark;
    return $self->{mark_package};
}


sub relativeSrcFilename {
    my( $self, $name ) = @_;
    $self->{src_relative_filename} = $name  if defined $name;
    return $self->{src_relative_filename};
}

sub absoluteSrcFilename {
    my( $self, $name ) = @_;
    $self->{src_absolute_filename} = $name  if defined $name;
    return $self->{src_absolute_filename};
}

sub srcBasename {
    my( $self, $name ) = @_;
    ( undef, undef, $self->{src_basename} ) = File::Spec->splitpath( $name )
        if defined $name;
    return $self->{src_basename};
}



1;
