package DoxygenToPod;

#** @class DoxgenToPod
#
# Extract doxygen documentation from perl sources and convert them to POD.
#
# The extracted and converted content is written to STDOUT or a file.  The POD
# documentation you're currently reading (possibly) is generated from Doxygen
# doc blocks in the sources of DoxygenToPod.pm.
#
# @author Pommes Schranke <chipsbarrier@gmail.com>
#*

use strict;
use utf8;
use feature qw'say state';
use Carp qw/ carp croak /;
use Cwd 'abs_path';
use File::Basename 'fileparse';
use Data::Dumper;
use IO::File;
use Doxygen::DocBlock;

our $VERSION = '0.01';


#** @method public new ( %params )
#
# Create a new Instance of DoxgenToPod.
#
# @param params  Hash of named parameters, most of which are optional:
# <pre>
# DoxygenToPod->new (
#     src          => $src_file,      # REQUIRED: Path to Doxygen documented file
#     pod          => $pod_file,
#     output_under => $output_directory,
#     overwrite    => $overwrite_existing_podfiles,
#     perms        => $permissions_of_podfile,
#     sigils       => $add_sigils_to_parameter_names,
#     formatter    => Doxygen::Formatter->forString('org'),
# );
# </pre>
#
# @see DoxygenToPod::setOverH1()
# @see DoxygenToPod::setOverH2()
# @see DoxygenToPod::setOverList()
#*
sub new {

    my( $class, %params ) = @_;

    $params{perms} ||= 0644;

    my $self = {
        in => IO::File->new(),
        out => IO::File->new(),
        current_package => undef,
        overwrite => undef,
        parameter_tags_have_sigils => 0,
    };
    bless $self, $class;

    {
        croak 'No source file given!'               unless    $params{src};
        croak "'$params{src}': File not readable!"  unless -r $params{src};

        $self->srcFile( $params{src} );
        my( $name, $dirs, $ext ) = fileparse $params{src}, qr/\.p[ml]/i;
        if( '.pl' eq lc $ext ) {
            $self->_currentPackage( $name . $ext );
        }
    }

    $self->overwriteExisting( $params{overwrite} ) if $params{overwrite};
    $self->podFile( $params{pod} ) if $params{pod};
    $self->podPerms( $params{perms} );
    $self->outputDirectory( $params{output_under} );
    my $formatter = $self->formatter( $params{formatter} )
        or croak( 'No formatter given!' );


    $self->setAddSigilToParamDescription(
        $params{sigils}
        ) if exists $params{sigils};

    return $self;
}

sub DESTROY {
    my $self = shift;
    if( $self->{in}->opened() ) {
        eval { $self->{in}->close() }   or warn $!;
    }
    if( $self->{out}->opened() ) {
        eval { $self->{out}->close() }   or warn $!;
    }
}

sub formatter {
    my( $self, $formatter ) = @_;

    $self->{formatter} = $formatter   if $formatter;
    return $self->{formatter};
}


#** @method public srcFile ( $src = undef )
#
# Get or set the source file - the *.pm file that is.
#
# @param src Path to the source file.  Has to exist if given.
# @return Absolute path to the source file.
#*
sub srcFile {                           # get or set the src file

    my( $self, $src ) = @_;
    if( defined $src )
    {
        croak( "$src: No such source file" )   unless -f $src;
        $self->{src} = abs_path($src);
    }
    return $self->{src};
}

#** @method overwriteExisting ( $overwrite = undef )
#
# Set or get the will to overwrite existing POD files.
#
# @return True, if existing POD files are to be overwritten, false otherwise.
#*
sub overwriteExisting {

    my( $self,  $overwrite ) = @_;
    if( defined $overwrite ) {
        $self->{overwrite} = ( $overwrite ? 1 : 0 );
    }
    return $self->{overwrite};
}

#** @method outputDirectory ( $directory = undef )
#
# Set or get the output directory.
#
# @param directory Path to the output directory.  A trailing slash will be
#                  appended.
# @return The path to the output directory as given in the constructor with it's
#         trailing slash, or the empty string.
#*
sub outputDirectory {

    my( $self, $dir ) = @_;
    if( $dir )
    {
        -d $dir or croak("$dir: Directory not found");
        -w $dir or croak("$dir: Directory not writable");
        $dir = File::Spec->rel2abs( "$dir" );
        ( $dir = "$dir/" ) =~ s<//$></>o; # Docs do not state, if slash is appended...
        $self->{outputDirectory} = $dir;
    }
    return $self->{outputDirectory};
}

#** @method public podFile ( $pod = undef )
#
# Get or set the output file or filehandle.
#
# @param pod Path to the output file or some object, that \c fileno() can
#            handle.
#
# @return Path or file handle to write the output to.
#
# @TODO: Denote the podFile according to the current module (and respect the
# pod-directory).
#*
sub podFile {                           # get or set the pod file

    my( $self, $pod ) = @_;
    $self->{pod} = $pod         if( $pod );
    return $self->{pod};
}

#** @method originalFh ( $original = undef )
#
# Store the original output filehandle to restore it later.
#
# @param original The original output file handle.
# @return The original output file handle.
#*
sub originalFh {

    my( $self, $original ) = @_;
    if( $original ) {
        $self->{originalFh} = $original;
    }
    elsif( ! defined $self->{originalFh} ) {
        croak( "Orignal filehandle not yet set but queried!" );
    }
    return $self->{originalFh};
}

#** @method podPerms ( $perms = undef )
#
# Getter-setter for output file permissions.
#
# @param perms Permissions for output files.  Default: \c 0644
# @return  Permissions for output files.
#*
sub podPerms {

    my( $self, $perms ) = @_;
    $self->{perms} = $perms         if( $perms );
    return $self->{perms};
}

#** @method setAddSigilToParamDescription ( $bool = undef )
#
# By default, doxygen's \@param tags do not carry sigils.  Set this to a true
# value, to add them anyway.
#
# @param bool  If true, add sigils.
#*
sub setAddSigilToParamDescription {
    my( $self, $add_sigils ) = @_;
    $self->{parameter_tags_have_sigils} = ( $add_sigils ? 1 : 0 );
}

#** @method private _currentPackage ( $pkg = undef )
#
# Getter-setter for the name of the package currently parsed.
#
# @param pkg Packagename.
# @return The name of the package currently parsed.
#*
sub _currentPackage {

    my( $self, $pkg ) = @_;
    $self->{current_package} = $pkg       if $pkg;
    return $self->{current_package};
}

#** @method _perlFileName ( $self, $module = undef )
#
# Generate and/or give a filename for display.  The path is calculated based on
# the documented module and the file system path.  The calculation might fail,
# since you may define a Perl module in just about any file you like, in which
# case the file system path is used.
#
# @param module The name of the current module.  If given, the path is
#               (re-)calculated.  Must be called at least once.
# @return The filename as displayed in the output.
#*
sub _perlFileName {
    my( $self, $module ) = @_;

    unless( $module ) {
        ( $module = $self->srcFile ) =~ s/.*\/// ;
        $module =~ s/\.[^.]+$//;
        carp $self->srcFile
            . ": No module name found! Using source file's basename '$module'.";
    }

    my $m_base = ( split( '::', $module ) )[0];
    my @f_parts = split( /[\\\/]/o, $self->srcFile );
    $self->{perlFileName} = $f_parts[ -1 ]; # Fallback to basename.
    shift @f_parts   while @f_parts && $f_parts[0] !~ /^$m_base/;
    if( @f_parts ) {
        $self->{perlFileName} = join( '/', @f_parts );
    }
    return $self->{perlFileName};
}


#** @method doxygenToPod ()
#
# Parse doxygen comments, write POD.  This is the only method the user needs to
# call after construction.
#*
sub doxygenToPod {

    my( $self ) = @_;

    # TODO: Open temp file and move it when done.  The user might request
    # several *.pod files for mulitple modules defined in one src file...
    # somewhen in the future that is....
    my $fh = $self->podFile;
    unless( defined(fileno($fh)) )
    {
        if( -f $fh ) {
            croak "Refused to overwrite $fh"
                unless $self->overwriteExisting;
        }
        $self->{out}->open(
            $self->podFile, "w", $self->podPerms
            )                                       or croak $!;
        $fh = $self->{out};
    }
    $self->originalFh( select($fh) );

    $self->{in}->open( $self->srcFile, "r" )    or croak "Failed to open $self->{src} for reading: $!";

    # $dox = {
    #     package => 'PACKAGE_NAME',
    #     name => 'ELEMENT_NAME',
    #     type => 'class|file|function|method|var',
    #     doc_line => 'LINE_OF_DOXYGEN_START',
    #     def_line => 'LINE_OF_DEFINITION',
    #     doc => [ 'Main comment. First sentence is BRIEF.\n', ... ],
    #     params => [ {name => 'string', line => LINE_NR, doc => [ 'documentation...', ... ]}, ... ],
    #     seek_start => 3261,
    #     seek_end => 3735,
    # }

    my $file_dox = undef;
    my %packages;                                    # Packages might be mixed in src file.
    while( my $dox = $self->_nextDocBlock )
    {
        my $p = $dox->package;
        if( $dox->type eq 'function' ) {
            if( $dox->private ) {
                push @{$packages{$p}{'PRIVATE FUNCTIONS'}}, $dox;
            } else {
                push @{$packages{$p}{FUNCTIONS}}, $dox;
            }
        }
        elsif( $dox->type eq 'method' ) {
            if( $dox->private ) {
                push @{$packages{$p}{'PRIVATE METHODS'}}, $dox;
            } else {
                push @{$packages{$p}{'METHODS'}}, $dox;
            }

        }
        elsif( $dox->type eq 'constructor' ) {
            push @{$packages{$p}{'CONSTRUCTORS'}}, $dox;
        }
        elsif( $dox->type eq 'file' ) {
            $file_dox = $dox;
        }
        elsif( $dox->type eq 'class' ) {
            $packages{$p}{PACKAGE} = $dox;
        }
        elsif( $dox->type eq 'var' ) {
            push @{$packages{$p}{VARS}}, $dox;
        }
    }
    # warn Dumper( \%packages );

    my $src_filename = $self->_perlFileName( $self->_currentPackage );
    $self->formatter->setSourceFilename(
        rel => $src_filename,
        abs => $self->srcFile,
    );

    $self->formatter->beginDocument();
    $self->formatter->formatFile( $file_dox )
        if $file_dox;

    # To create custom IDs for headlines, packages need to know if there are
    # more than one package documented in the output file:
    my @package_names = keys %packages;
    my $mark_package = scalar(@package_names) > 1 ? 1 : 0;
    for my $p ( @package_names )
    {
        $self->formatter->setCurrentPackage($p, $mark_package);

        if( $packages{$p}{PACKAGE} ) {
            $self->_formatPackage( $packages{$p}{PACKAGE} );
        }
        if( $packages{$p}{CONSTRUCTORS} ) {
            $self->_formatSubroutines( $packages{$p}{CONSTRUCTORS}, 'CONSTRUCTORS' );
        }
        if( $packages{$p}{FUNCTIONS} ) {
            $self->_formatSubroutines( $packages{$p}{FUNCTIONS}, 'FUNCTIONS' );
        }
        if( $packages{$p}{METHODS} ) {
            $self->_formatSubroutines( $packages{$p}{METHODS}, 'METHODS' );
        }
        if( $packages{$p}{'PRIVATE FUNCTIONS'} ) {
            $self->_formatSubroutines( $packages{$p}{'PRIVATE FUNCTIONS'}, 'PRIVATE FUNCTIONS' );
        }
        if( $packages{$p}{'PRIVATE METHODS'} ) {
            $self->_formatSubroutines( $packages{$p}{'PRIVATE METHODS'}, 'PRIVATE METHODS' );
        }
        if( $packages{$p}{VARS} ) {
            $self->_formatVariables( $packages{$p}{VARS} );
        }
    }

    $self->formatter->endDocument();

    # my @classes = grep { $_[0][0]->{type} == 'class' } @documentation;
    # my @files   = grep { $_[0][0]->{type} == 'file'  } @documentation;
    # my @subs    = grep { $_[0][0]->{type} == 'sub'   } @documentation;
    # my @vars    = grep { $_[0][0]->{type} == 'var'   } @documentation;

    select $self->originalFh;

}


#### OUTPUT ####
sub _formatPackage {

    my( $self, $p ) = @_;
    $self->formatter->formatPackage($p);
}


sub _formatSubroutines {

    my( $self, $aref, $headline ) = @_;
    $self->formatter->formatSection( $headline );
    $self->_sortElements( $aref );
    for my $dox ( @$aref ) {
        $self->formatter->formatSubroutine( $dox );
    }
}


sub _sortElements {

    my( $self, $aref ) = @_;
    @$aref = sort {
        my $private = $a->{private}     <=> $b->{private};
        my $name    = $a->{name} cmp $b->{name};
        return $private if $private;
        return $name;
    } @$aref;
}


#### FORMAT UNTAGGED DOXYGEN ####
sub _formatDoxygen {

    my( $self, $dox ) = @_;
    return unless $dox;
    $self->formatter->formatDoxygen($dox);
}



#### FORMAT VARIABLES ####
sub _formatVariables {

    my( $self, $aref ) = @_;
    $self->_sortElements( $aref );
    $self->formatter->formatSection('VARIABLES');
    for my $dox ( @$aref ) {
        $self->formatter->formatVariable($dox);
    }
}

#** @function _extractSigil ( \@header_parameters, $parameter_name )
#
# Extract the sigil from a list of header parameters (i.e. the list extracted
# from a functions/methods docblock-header).
#
# @param header_parameters Parameter list extracted from a functions/methods
#                          docblock-header.
# @param parameter_name    Name of the parameter the sigil is for.
# @return The sigil or an empty string.
#*
sub _extractSigil {
    my( $header_params, $pname ) =  @_;
    local $1;
    for my $hp ( @{$header_params} ) {
        return $1 if $hp =~ /^([\\\$@%&]+)${pname}\b/;
    }
    return '';
}



#### BLOCK ITERATORS ####

sub _nextDocBlock {

    my $self = shift;

    my $line;
    my $pkg = 'main';
    my $in = $self->{in};
    my $pos = $in->input_line_number;

    while( defined( $line = <$in> ) )
    {
        chomp $line;
        if( $line =~ /^package (\w+(?:::\w+)*);\h*(?: #.*)?$/ )
        {
            $self->_currentPackage( $1 );
        }
        elsif( $line =~ /^\s*#\*\*/ )
        {
            my $last_comment_line_start = $in->tell();
            my $last_comment_line_nr    = $in->input_line_number();

            my @block = ( $line );
            while( defined( $line = <$in> ) )
            {
                last if $line =~ /^\s*#\*+\s*$/o;
                if( $line =~ /^\s*[^#\s].*/o )
                {
                    warn '[WARN] "#*" missing at end of comment block in '
                        . $self->srcFile
                        . ' line ' . $last_comment_line_nr;
                    $in->seek($last_comment_line_start, SEEK_SET);
                    last;
                }
                push @block, $line;

                $last_comment_line_start = $in->tell(); # Last commentline.
                $last_comment_line_nr    = $in->input_line_number();
            }
            my $end = $in->input_line_number;
            my $dox = $self->_analyseDocBlock( \@block, $pos, $end );
            next unless $dox;                                   # TODO: Message to stderr

            # TODO: Try to find the definition...
            $dox->def_line( $in->input_line_number + 1 );
            return $dox;
        }
        $pos = $in->input_line_number;
    }
}

#** @method _analyseDocBlock ( $block, $first_line, $last_line )
#
# Analyse a Doxygen comment block.
#
# @param block      The block to analyze. An array ref of source lines.
# @param first_line The input line number of the first line in $block.
# @param last_line  The input line number of the last line in $block.
#
# @return A Doxygen::DocBlock.
# <pre>
# {
#     first_line => 75,                 # first comment line
#     last_line => 84,                  # last comment line
#     def_line => 85,                   # line where element is defined
#     type => 'method',                 # 'method', 'function', 'class' etc.
#     private => 0,                     # 0 or 1
#     name => 'functionName',    # name as found in the docblock header
#     header_params => [ '$parameterName = undef' ], # params in docblock header
#     doc => [ "line\n", "line\n", ],   # documentation body
#     package => 'CurrentPerlPackage',  # a file can hold more than one package
#     # The rest is sub routines only:
#     params => [                       # parsed parameters
#         {
#             doc  => [ "line\n", ],    # @param docs
#             name => 'parameterName',  # name w.o. sigil
#             line => 79,               # input line number @param
#         }],
#     RETURN => [ "line\n", "...\n", ], # @return docs
# }
# </pre>
#*
sub _analyseDocBlock {

    my( $self, $block, $first_line, $last_line ) = @_;

    my $comment_rx = qr/^\s*#/o;
    my $type_rx    = qr/\h*\@(class|file|constructor|function|method|var)\b/o;
    my $vis_rx     = qr/(private|public)\b/o;
    my $name_rx    = qr/([-\@\%\$\w0-9:.\/]+)/o; # Might be a filename
    my $params_rx  = qr/\((.*?)\)?/o;
    my $tags_rx    = qr/^\h*\@(todo|note|return|see|throws|warning|author|copyright)\b/o;
    my $ltrim_rx   = qr/^\h*#\h?/;

    local( $1, $2, $3, $4 );

    return undef
        unless $block->[0] =~
        /^\h*#\*\*\h+$type_rx(?:\h+$vis_rx)?(?:\h+$name_rx)?(?:\h+$params_rx)?\h*$/o;

    my( $type, $params );
    my( $mtype, $mvis, $mname, $mparams ) = ($1, $2, $3, $4);
    unless( $mname ) {                      # 'public' is a valid name!
        $mname = $mvis || '';
        $mvis = '';
    }
    unless( $self->_currentPackage() )
    {
        if( 'class' eq $mtype ) {
            $self->_currentPackage($mname);
        }
        else {
            die "Dokumentation starts before we know which package we're in!"
        }
    }
    my $dox = Doxygen::DocBlock->new(
        package     => $self->_currentPackage(),
        type        => $mtype,
        first_line  => $first_line,
        last_line   => $last_line,
        name        => $mname,
    );

    # Handle missing `public|private' obeying Perl conventions:
    if( ($mvis eq 'private') || 0 == index($dox->name, '_') ) {
        $dox->private(1);
    }

    $mparams =~ s/^\h+|\h+$//g;
    $dox->{header_params} = [(
        map { $_ =~ s/^\s+|\s+$//g; $_ } split(/,\s*/, $mparams)
        )]  if $mparams;

    shift @$block;                      # Remove header line.

    my $linenr = $dox->first_line;
    # We leave an empty line in.  That way the user/formatter may decide.
    while( my $line = shift @$block )
    {
        ++$linenr;
        last unless $line =~ /$comment_rx/;
        $line =~ s/$ltrim_rx//;
        if( $line =~ /^\h*\@param\s+([\w0-9]+)/o )
        {
            my $pname = $1;
            my $pdoc = {
                name => $pname,
                sigil => '',
                line => $linenr,
            };
            if( $self->{parameter_tags_have_sigils} ) {
                $pdoc->{sigil} = _extractSigil( $dox->{header_params}, $pname )
                    or warn
                    '[WARN] Possible typo found in '
                    . $self->srcFile . ' line '
                    . ($linenr + 1) .
                    ": parameter '$pname' not found in \@function or \@method line!\n";
            }
            $line =~ s/^\h*\@param\s+([\w0-9]+)\h+//o;
            $pdoc->{doc} = [ $line ];
            while( defined $block->[0]
                   && $block->[0] !~ /^\h*\#\h*\@/
                   && $block->[0] =~ /$comment_rx/)
            {
                ++$linenr;
                $line = shift @$block;
                $line =~ s/$ltrim_rx//;
                push @{$pdoc->{doc}}, $line;
            }
            $dox->addParam( $pdoc );
            next;
        }

        elsif( $line =~ /$tags_rx/ )
        {
            my $tag = lc($1);
            $line =~ s/^\@[a-z]+\s//o;
            my $blk = [ $line ];
            while( defined $block->[0]
                       && $block->[0] !~ /^\h*\#\h*\@/ )
            {
                ++$linenr;
                $line = shift @$block;
                $line =~ s/$ltrim_rx//;
                push @{$blk}, $line;
            }
            $dox->addTag($tag, $blk);
            next;
        }

        else
        {
            $dox->addDocLine( $line );
        }
    }

    return $dox;
}



#### PROCESSING TEXT FOR OUTPUT ####



1;
