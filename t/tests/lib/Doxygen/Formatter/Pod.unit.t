use strict;
use utf8;
use Test2::V0;
use Test2::Tools::Exception;
use Cwd 'abs_path';

use Doxygen::Formatter::Pod;

my $dataDir = 't/data';                 # no slash please!


subtest 'test_emph' => sub
{
    my $A = '<<< ';
    my $B= ' >>>';

    my @tests = (
        #### bold stuff ####
        # \b
        { in => "\\b word end\n", out => "B${A}word${B} end\n" },
        { in => "bold \\b My::Mod end\n", out => "bold B${A}My::Mod${B} end\n" },
        # @b
        { in => "\@b word end\n", out => "B${A}word${B} end\n" },
        { in => "bold \@b My::Mod end\n", out => "bold B${A}My::Mod${B} end\n" },
        # <b>
        { in => "<b>word</b> end\n", out => "B${A}word${B} end\n" },
        { in => "bold <b>My::Mod and more</b> end\n", out => "bold B${A}My::Mod and more${B} end\n" },

        #### code and tt stuff ####
        # \c
        { in => "\\c word end\n", out => "C${A}word${B} end\n" },
        { in => "pre \\c My::Mod end\n", out => "pre C${A}My::Mod${B} end\n" },
        # @c
        { in => "\@c word end\n", out => "C${A}word${B} end\n" },
        { in => "pre \@c My::Mod end\n", out => "pre C${A}My::Mod${B} end\n" },
        # <code>
        { in => "<code>word end</code>\n", out => "C${A}word end${B}\n" },
        { in => "word <code>My::Mod</code>\n", out => "word C${A}My::Mod${B}\n" },
        # <tt>
        { in => "<tt>word</tt> end\n", out => "C${A}word${B} end\n" },
        { in => "pre <tt>My::Mod and more</tt> end\n", out => "pre C${A}My::Mod and more${B} end\n" },

        #### italic stuff ####
        # \a
        { in => "\\a italic via \\\\a\n", out => "I${A}italic${B} via \\a\n" },
        { in => "italic via \\a \\\\a end\n", out => "italic via I${A}\\a${B} end\n" },
        # @a
        { in => "\@a \@a italic\n", out => "I${A}\@a${B} italic\n" },
        { in => "italic \@a at a\n", out => "italic I${A}at${B} a\n" },
        # \e
        { in => "\\e italic via \\\\e\n", out => "I${A}italic${B} via \\e\n" },
        { in => "italic via \\e \\\\e end\n", out => "italic via I${A}\\e${B} end\n" },
        # @e
        { in => "\@e italic via \\\@e\n", out => "I${A}italic${B} via \@e\n" },
        { in => "italic via \@e at e\n", out => "italic via I${A}at${B} e\n" },
        # \em
        { in => "\\em italic via \\\\em\n", out => "I${A}italic${B} via \\em\n" },
        { in => "italic via \\e \\\\em end\n", out => "italic via I${A}\\em${B} end\n" },
        # @em
        { in => "\@em italic via \\\@em\n", out => "I${A}italic${B} via \@em\n" },
        { in => "italic via \@em \@em end\n", out => "italic via I${A}\@em${B} end\n" },
        # <i>
        { in => "<i>italic</i> per HTML\n", out => "I${A}italic${B} per HTML\n" },
        { in => "italic <i>My::Mod and more</i> end\n", out => "italic I${A}My::Mod and more${B} end\n" },

        #### underlined stuff ####
        # \u
        { in => "\\u word end\n", out => "U${A}word${B} end\n" },
        { in => "underline \\u My::Mod end\n", out => "underline U${A}My::Mod${B} end\n" },
        # @u
        { in => "\@u word end\n", out => "U${A}word${B} end\n" },
        { in => "underline \@u My::Mod end\n", out => "underline U${A}My::Mod${B} end\n" },
        # <u>
        { in => "<u>word</u> end\n", out => "U${A}word${B} end\n" },
        { in => "underline <u>My::Mod and more</u> end\n", out => "underline U${A}My::Mod and more${B} end\n" },
        );

    for my $t ( @tests )
    {
        is( Doxygen::Formatter::Pod::emph( $t->{in}), $t->{out},  "$t->{in}  ==>  $t->{out}" );
    }
};


done_testing;
