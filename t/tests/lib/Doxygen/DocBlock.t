use strict;
use Test2::V0;
use Test2::Tools::Exception;


use Doxygen::DocBlock;

my $expected_brief = 'This is the brief for our docblock and it spans
two lines.';
my $doc = [
    "This is the brief for our docblock and it spans\n",
    "two lines. It's followed by more text.\n",
    "and even more so.\n"
];



subtest 'test_Doxygen::DocBlock->new' => sub {

    my $db;
    ok lives {
        $db = Doxygen::DocBlock->new(
            package => 'Doxygen::DocBlock',
            name => '_build_brief',
            type => 'method',
            first_line => 261,
            'doc' => $doc,
        );
    }, 'Create a DocBlock with the minimum of parameters required';

    is $db->brief, $expected_brief, 'Brief as expected';
    is $db->private, 0, 'Not private by default';
    $db->private(1);
    is $db->private, 1, '...you may make it private so';
};


subtest 'test_Doxygen::DocBlock->_trim' => sub {

    my @tests = (
        {
            val => " \n ABC\nDEF g. \n ",
            expected => "ABC\nDEF g.",
        },
    );

    for my $t ( @tests )
    {
        is( Doxygen::DocBlock->_trim( $t->{val} ), $t->{expected} );
    }

};




done_testing();
