use strict;
use utf8;
use Test2::V0;
use Test2::Tools::Exception;
use Cwd 'abs_path';

use Doxygen::Formatter;
use Doxygen::Formatter::Pod;
use DoxygenToPod;


my $dataDir = 't/data';                 # no slash please!



subtest test_srcFile => sub {

    my $abs = abs_path( __FILE__ );

    my $non_existing_file = 'hackepeterKneter.pm';

    ok dies { _createInstance(); };
    ok dies { _createInstance(undef); };
    ok dies { _createInstance(undef, __FILE__ . '.pod'); };
    ok dies { _createInstance($non_existing_file); };

    my $instance = _createInstance( __FILE__, undef );
    is $instance->srcFile, $abs, 'srcFile set to the absolute path';
    is $instance->podFile, undef, 'No podFile in Konstruktor, empty podFile field';

    $instance->srcFile(__FILE__);
    is $instance->srcFile, $abs, 'srcFile set again';
    ok dies { $instance->srcFile( '' ); };

    $instance->srcFile(__FILE__);
    is $instance->srcFile, $abs, 'srcFile set again and again';
    ok dies { $instance->srcFile( 0 );  };

    $instance->srcFile(__FILE__);
    is $instance->srcFile, $abs, 'srcFile set again and again and again';
    ok dies { $instance->srcFile( $non_existing_file );  };
};


subtest test_podFile => sub {

    my $pod_file = __FILE__ . '.pod';
    my $instance = _createInstance(__FILE__);
    $instance->podFile( $pod_file );
    is $instance->podFile, $pod_file, 'podFile field set';

    $instance->podFile( undef );
    is $instance->podFile, $pod_file, 'podFile( undef ) does not change podFile field';

    $instance->podFile( $pod_file );
    is $instance->podFile, $pod_file, 'podFile set again';
    $instance->podFile( '' );
    is $instance->podFile, $pod_file, 'podFile( "" ) does not change podFile field';

    $instance->podFile( $pod_file );
    is $instance->podFile, $pod_file, 'podFile set again and again';
    $instance->podFile( 0 );
    is $instance->podFile, $pod_file, 'podFile( 0 ) does not change podFile field';

};


# Might become complicated to create an instance of DoxygenToPod.
# @param src  Source file - optional.
# @param pod  Pod file    - optional as well.
sub  _createInstance {

    my( $src, $pod ) = @_;
    my $formatter = Doxygen::Formatter->forString('pod');
    return DoxygenToPod->new( src => $src, formatter => $formatter,  pod => $pod );
}



done_testing;
