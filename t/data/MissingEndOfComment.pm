package MissingEndOfComment;


#** @class MissingEndOfComment
#
# Doxygen comments with wrong endings.
# This comment block ends in `#**' (instead of `#*').
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#**


use strict;
use utf8;
use Carp 'carp';


#** @method new ( )
# Create a new Instance of MissingEndOfComment.
# This comment block does not end in an asterisk.
#
sub new
{
    my( $class, ) = @_;
    my $self = {};
    bless $self, $class;
}

#** @method x ( $y )
# Do x with <code>$y</code>.
# @param y  The y to handle x-ish.
#*
sub x {
    my( $self,  $y  ) = @_;
}

1;
