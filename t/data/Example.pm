package Org::Cache;


#** @class Org::Cache
#
# Publishing cache for Org stuff.
# This version of the cache reads the cache from a gzip file on construction.
#
# \b Usage:
# <pre>
# my $cache = Org::Cache->getInstance( $config );
# if( my $ret = $cache->resourceLookup( $path, $render_type ) )
# {
#     return $ret;
# }
# # ... Render a resource not yet in Cache ...
# </pre>
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*



#** @method getInstance ( $org_config )
#
# Get or create an Instance of Org::Cache.
#
# @param org_config An Instance of Org::Config.
#*
sub getInstance { }


# Write cache to file.
sub DESTROY { }


#** @method _readCacheFromFile ( )
#
# Read the cache from file.  If the file does not exist yet, set up the
# default cache structure.  DO NOT CALL THIS FUNCTION from somewhere else.
#*
sub _readCacheFromFile { }

#** @method _writeCacheToFile ( )
#
# Write the cache to file.  This method does not die, if the directory or file
# is not writable.  We just write a warning to the logfile and return.  The
# cache would be gone otherwise.  Never call this one!  The cache is written on
# destruction.
#
# @return  TRUE on success, FALSE otherwise.
#*
sub _writeCacheToFile { }

sub undocumentedSub {
    return $_[0]->{resourceDirectory};
}


sub resourceCacheName { }


#** @method resourceLookUp ( $path, $render_type )
#
# Look up a resource in the cache.  The files are cached based on their
# cannonical path and the requested rendering type.
#
# @param  path         An Org::Path object.
# @param  rendering_type Requested rendering type.
# @return The cached resource as PSGI-Response, if valid version of the resource
# is found in the cache.  Undef otherwise.
# <pre>
# {
#     name => 'value',
# };
# </pre>
#*
sub resourceLookUp {}


#** @method addResource ( $path, $response, $render_type )
#
# Add a resource to the cache.  This is, what ContentHandlers call to cache
# their results.  There's no use in adding static resources to the cache.
#
# @param path  An Org::Path
# @param response PSGI-like response to serialize.
# @param render_type Render-type.  Denotes the subdirectory to cache in.
# @return \c True, if the file was cached, <code>false</code> otherwise.  It's not an error,
#         if the file was not cached since another worker could lock the
#         cache.
#*
sub addResource
{}



#** @method cacheDirectory ()
#
# Return the Cache Directory.
#
# @return  Path to the cache directory.
#*
sub cacheDirectory
{
    $_[0]->{cacheDirectory};
}

1;
