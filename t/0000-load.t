use 5.018;
use 5.14.0;
use 5.16.0;
use Test2::V0;

diag( "Testing DoxygenToPod $DoxygenToPod::VERSION, Perl $], $^X" );

my @requirements = qw( Carp.pm
          Carp/Assert.pm
          Cwd.pm
          Data/Dumper.pm
          File/Basename.pm
          File/Util.pm
          FindBin.pm
          IO/File.pm
          IO/Handle.pm
          Getopt/Long.pm
          Pod/Usage.pm
          Test2/V0.pm
          Test2/Tools/Exception.pm
          DoxygenToPod.pm
    );

for ( @requirements )
{
    my $found = eval { require( $_ ) };
    s/\.pm$//;
    s/\//::/g;
    ok $found, "Found Perl module $_";
}


done_testing();
