#!/usr/bin/env perl

use strict;
use utf8;

use Cwd ();
use Carp::Assert;
use Data::Dumper;
use File::Basename qw/ basename /;
use File::Util;
use Getopt::Long qw/:config gnu_getopt/;
use feature qw/ say state /;

use FindBin qw/ $RealBin /;
use lib "$RealBin/../lib";
use DoxygenToPod;
use Pod::Usage;
use Doxygen::Formatter;


## globals:
my $Program = basename $0;
my $Recurse;
my $CanonicalNames = 0;
my $ToPodfile = 0;
my $NoSigils  = 0;
my $ForceOverwrite;
my $OutputDirectory;
my $OutputFormat = 'Pod';
my @Files;


GetOptions(
    'help|h|?' => sub {
        pod2usage( -exitval => 0, -verbose => 2 )
    },
    'recursive|r'  => \$Recurse,
    'canonical|c'  => \$CanonicalNames,
    'to-podfile|P' => \$ToPodfile,
    'no-sigils|n'  => \$NoSigils,
    'output-under|output|D|o=s' => \$OutputDirectory,
    'force-overwrite|force|F' => \$ForceOverwrite,
    'output-format|format|f=s' => \$OutputFormat,
)
    or do {
        pod2usage(1);
    };

# Rest is considered filenames and directories:
while( my $f = pop )
{
    if( -d $f ) {
        push_dirs( $f );
    }
    else {
        push_files( $f );
    }
}


unless( scalar @Files ) {
    pod2usage( -exitval => 1, -msg => 'No input files given' );
}

# warn Dumper( \@Files )  if DEBUG;


exit main(\@Files);

##### END OF PROGRAM


sub main {

    my $files = pop();
    my $dest  = \*STDOUT;
    my $formatter = Doxygen::Formatter->forString($OutputFormat);
    my $ext = $formatter->outputFilenameExtension();

    for my $src ( @$files )
    {
        if( $ToPodfile ) {
            $dest = $src;
            # The default is, to swap '.pm' for '.pod'.  If the output directory
            # is given, the path is recalculated in Doxygen2Pod.pm.
            $dest =~ s/\.p[lm]$/\.$ext/i;
        }

        my $d2p = DoxygenToPod->new(
            src => $src,
            pod => $dest,
            formatter => $formatter,
            sigils => ( $NoSigils ? 0 : 1 ),
            overwrite => $ForceOverwrite,
            output_under => $OutputDirectory,
        );

        $d2p->doxygenToPod();

        if( $ToPodfile ) {
            say "DONE: $dest";
        }
    }
}


sub push_dirs {

    state $futil = File::Util->new();
    my @dirs = @_;
    my @files;
    for my $d ( @dirs )
    {
        pod2usage("$d: Not a directory") unless -d $d;

        push @files, ( $futil->list_dir(
            "$d/" => {
                files_match => qr/^[^.#].*?\.p[lm]$/i,
                files_only  => 1,
                recurse     => 1,
                max_depth   => ( $Recurse ? 0 : 1 ),
            }));
    }
    push_files( @files );
}


sub push_files {

    my @fs = @_;
    for my $f ( @fs )
    {
        pod2usage("$f: File not found")  unless -f $f;

        $f = Cwd::abs_path( $f )   if $CanonicalNames;

        pod2usage("$f: Failed to canonicalize filename") unless $f;
        push @Files, $f;
    }
}


# use File::Path qw(make_path);
# sub podDirectory {
#     my $directory = shift;
#     unless( -d $directory ) {
#         make_path( $directory )
#             or croak("$directory: Failed to create directory for pod output: $!");
#     }
#     return abs_path( $directory );
# }


__END__

=encoding utf8

=head1 NAME

=over 8

doxyge2pod

=back

=head1 USAGE

    PROG [ -?FhnPr ] FILE_OR_DIRECTORY ...

=head1 DESCRIPTION

doxygen2pod translates Doxygen comments to POD.  By default, all output is
written to C<STDOUT>.  You may choose to place the POD for (e.g.)
C<My::Fancy::Modul.pm> in another file named C<My/Fancy/Modul.pod> (see option
C<-P>) and more.


=head1 OPTIONS

=over 8

=item C<--help> | C<-h> | C<-?>

Display this help and exit.

=item I<FILE_OR_DIRECTORY>

All non-option parameters are considered files or directories for input.  All
files given are processed.  For directories, read and translate all Perl modules
found in I<FILE_OR_DIRECTORY>.  By default, this is done non-recursive, but see
C<--recursive>.  You may provide this option multiple times to process multiple
files and directories.

=item C<--recurse> | C<-r>

Process each I<FILE_OR_DIRECTORY> recursively.  Non-recursive is the default.

=item C<--to-podfile> | C<-P>

For each input file, write POD to a file named I<BASENAME.pod> where I<BASENAME>
is the filename of the Perl module the produced POD stems from sans extension
I<p[lm]>.  The POD file is placed next to the Perl module file, unless an ouput
directory is supplied (see option -D).

Existing POD files are not overwritten, unless C<--clobber> is given.

=item C<--output-directory> I<DIR> | C<-D> I<DIR> | C<-o> I<DIR>

Implies -P. Write output to I<DIR>.  The name of the POD file is made from that
I<DIR> plus the module C<name =~ s/::/\//g>.  The directory is created
if it does not yet exist.  Use C<-f> to overwrite existing POD files.

=item C<--output-fromat> I<FORMAT> | C<--format> I<FORMAT> | C<-f> I<FORMAT>

Choose the output format.  Supported formats are 'pod', 'org'.  Defaults to 'pod'.

=item C<--force-overwrite> |  C<--force> | C<-F>

Overwrite existing POD files.

=item C<--no-sigils> | C<-n>

The C<@param> tags do not allow for the paramternames to carry sigils.  Thus
they are added for you automatically to the parameter description list.  You may
supply this option to skip the sigils.

=back

=cut

# =head1 OPTIONS PLANED BUT NOT YET IMPLEMENTED
#
# =over 8
#
# =item C<--after-end> | C<--in-file> | C<-i>
#
# Place all the POD documentation at the end of the files.  Inserts a new
# I<__END__> token if necessary.  Original contents following the I<__END__> token
# are overwritten!  This method will fail, if a I<__DATA__> section is found.
#
# =item C<--in-place> | C<-i>
#
# Replace the Doxygen comments themselfs with POD.  Like C<sed -i>.
#
#
# =back
#
# =cut
