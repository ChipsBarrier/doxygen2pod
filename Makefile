
# pm_file = t/data/Example.pm
pm_file = lib/DoxygenToPod.pm
pod_file = tmp.pod
raw_html = html/tmp.html
dark_html = html/pod-dark.html
light_html = html/pod-light.html
css = pod.css
highlightCssDir = highlight-js-perl-only/styles
highlightJsStyleLight = xcode.css
highlightJsStyleDark  = a11y-dark.css


all: pod

.proverc:
	@[ -l .proverc ] || ln -s _proverc .proverc

test: .proverc
	prove -r t

pod:
	bin/doxygen2pod.pl $(pm_file) > $(pod_file)
# bin/doxygen2pod.pl --no-sigils $(pm_file) > $(pod_file)

html:
	@[ -d html ] || mkdir html

pod2html: pod html
	pod2html --htmldir=./ --css $(css) --infile $(pod_file) --outfile $(raw_html)
# Does not produce an TOC:
#     perldoc -ohtml -F ./tmp.pod >tmp.html

html_pretty: html_light  html_dark

html_dark: pod2html
	cat css/head.html > $(dark_html)
	sed -i 's/HIGHLIGHTJS-STYLESHEET/$(highlightJsStyleDark)/' $(dark_html)
	sed -i 's/POD-STYLESHEET/pod-dark.css/' $(dark_html)
	sed -n '/^<body>/,10000p' $(raw_html) >> $(dark_html)

html_light: pod2html
	cat css/head.html > $(light_html)
	sed -i 's/HIGHLIGHTJS-STYLESHEET/$(highlightJsStyleLight)/' $(light_html)
	sed -i 's/POD-STYLESHEET/pod-light.css/' $(light_html)
	sed -n '/^<body>/,10000p' $(raw_html) >> $(light_html)

css-examples: pod2html
	@for f in $(highlightCssDir)/*.css; do \
	    bn=$${f##*/}; \
	    of=html/$${bn}.html; \
	    cat css/head.html > $$of ; \
	    sed -i "s/HIGHLIGHTJS-STYLESHEET/$$bn/" $$of; \
	    sed -i 's/POD-STYLESHEET/pod-light.css/' $$of; \
	    sed -n '/^<body>/,10000p' $(raw_html) >> $$of; \
	done

clean:
	@echo "  =" Making $@ in `pwd`; \
	for obj in `find ./ -name '*~'` \
		`find . -regextype posix-extended -regex '(.*/)?#[^/]+'`;\
		do echo  "    Removing  $$obj"; $(RM) "$$obj"; \
	done; \
	true

maintainer-clean: clean
	@rm -rf html_examples
	@git clean -fdx
